About
-----

Ansible role to install and configure pxe on a linux system.

Dependencies
-------------
ansible_role_dhcpd
ansible_role_tftp

Variables
---------
pxe_boot_path:
isolinux_url:
dhcp_clients:
  - hostname: client1
    ipv4: 192.168.70.10
    hwaddr: fa:16:3e:ef:8a:42
    group: linux
    pxe_config:
      kernel_path:
      initrd_path:
      kickstart_url:

Usage Example
-------------

```yml
pxe_boot_path: /tftpboot
isolinux_url: http://blazdata.rinconres.com/yum/7/os/x86_64/images/pxeboot/
dhcp_clients:
  - hostname: client1
    ipv4: 192.168.70.10
    hwaddr: fa:16:3e:ef:8a:42
    group: linux
    pxe_config:
      kernel_path: vmlinuz
      initrd_path: initrd.img
      kickstart_url: http://blazkickstart/configs/dynamic/backup.ks

roles:
  - { role: ansible_role_pxe, tags: pxe }
```


Testing
-------

```bash
molecule test
```
